package hoofdstuk5;

/**
 * Created By Moon
 * 1/8/2021, Fri
 **/
public class PriorityOfOperators {
    public static void main(String[] args) {

        int a = 5;
        int b = 2 ;
        System.out.println(a++ + --b);

         a = 1;
         b = 2 ;
        System.out.println(++a * b-- + b < 2 ? -a : ++b);

        a=1;
        b=1;
        System.out.println(++a * 2 + b < 2 ? -a : ++b);

        a=2;
        b=1;
        System.out.println(2 * 2 + b < 2 ? -a : ++b);

        a=2;
        b=1;
        System.out.println(4 + 1 < 2 ? -a : ++b);

        a=2;
        b=1;
        System.out.println(5 < 2 ? -a : ++b);

        a=2;
        b=1;
        System.out.println(false ? -a : ++b);

        a=2;
        b=2;
        System.out.println(false ? -a : 2);

        a=2;
        b=2;
        System.out.println(2);

    }
}
