package hoofdstuk5;

import java.util.Locale;
import java.util.Scanner;

/**
 * Created By Moon
 * 1/8/2021, Fri
 **/
public class ScrabbleWord {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please give a word :  ");
        String word = keyboard.next().toUpperCase();

        int score = 0;
        for (int i = 0; i < word.length(); i++) {
            char calculatedLetter = word.charAt(i);
            switch (calculatedLetter) {
                case 'A':
                case 'E':
                case 'I':
                case 'L':
                case 'N':
                case 'O':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                    score += 1;
                    break;
                case 'D':
                case 'G':
                    score += 2;
                    break;
                case 'B':
                case 'C':
                case 'M':
                case 'P':
                    score += 3;
                    break;
                case 'F':
                case 'H':
                case 'V':
                case 'W':
                case 'Y':
                    score += 4;
                    break;
                case 'K':
                    score += 5;
                    break;
                case 'J':
                case 'X':
                    score += 8;
                    break;
                case 'Q':
                case 'Z':
                    score += 10;
                    break;
                default:
                    break;
            }
        }
        System.out.println("The score for the " + word  + " is : " + score);
        keyboard.close();
    }

}
