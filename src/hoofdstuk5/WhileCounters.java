package hoofdstuk5;

import javax.security.sasl.SaslClient;
import java.util.Scanner;

/**
 * Created By Moon
 * 1/11/2021, Mon
 **/
public class WhileCounters {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int value;
        do {
            System.out.println("Give a number between 0 and 10: ");
            value = keyboard.nextInt();
        }while(value >= 0 && value <= 10);

/*
        int counter = 120;
        while(counter >= 100){
            System.out.println(counter);
            counter--;
        }
*/

        

/*        int number = 3;
        while(number < 50){
            System.out.println(number);
            number += 3;
        }*/

/*        int number2 = 5;
        while(number2 < 10000){
            System.out.println(number2);
            number2 *= 5;
        }*/

/*        char myChar = 'A';
        while(myChar <= 'Z'){
            System.out.println(myChar);
            myChar++;
        }*/

        keyboard.close();
    }
}
