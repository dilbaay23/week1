package hoofdstuk5;

import java.util.Scanner;

/**
 * Created By Moon
 * 1/8/2021, Fri
 **/
public class MonthDays {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please give a number of the month between 1 and 12 :  ");
        Integer monthAsNumber = keyboard.nextInt();
        int days;
        switch (monthAsNumber) {
            case 2:  days = 28;
            break;
            case 4:
            case 6:
            case 9:
            case 11: days = 30;
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12: days = 31;
            break;
            default: days=0;
            break;
        }
        System.out.println(days);

        keyboard.close();
    }
}
