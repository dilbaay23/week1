package hoofdstuk5;

import java.util.Scanner;

/**
 * Created By Moon
 * 1/8/2021, Fri
 **/
public class Arithmetic {
    public static void main(String[] args) {
        Scanner  keyboard = new Scanner(System.in);

        System.out.println("Write a number please : ");
        int first = keyboard.nextInt();

        System.out.println("Write another number please : ");
        int second = keyboard.nextInt();

        System.out.println("The sum of first and second = " + (first + second));
        System.out.println("The difference between first and second = " + (first - second));
        System.out.println("First * second = " + (first * second));
        System.out.println("First / second = " + (first / second));
        System.out.println("The mode of first and second = " + (first % second));

        keyboard.close();
    }
}
