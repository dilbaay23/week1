package hoofdstuk5;

import java.util.Scanner;

/**
 * Created By Moon
 * 1/8/2021, Fri
 **/
public class BMI {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("What is your  name? ");
        String name = keyboard.next();

        System.out.println("Please enter your height in cm: ");
        Double height = keyboard.nextDouble();

        System.out.println("Please enter your weight in kg: ");
        Double weight = keyboard.nextDouble();

        double bmi = (100 * 100 * weight) / (height * height);

        System.out.printf("Your BMI is %.2f" , bmi);

        if (bmi < 20) {
            System.out.println(name + " is Underweight");
        } else if (bmi < 25) {
            System.out.println(name + " is Healthy");
        } else if (bmi < 30) {
            System.out.println(name + " is Overweight");
        } else if (bmi < 40) {
            System.out.println(name + " is Obese");
        } else {
            System.out.println(name + " is sickly overweight");
        }
        System.out.printf("Your BMI is %.2f" , bmi);

        keyboard.close();

    }
}
