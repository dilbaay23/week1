package hoofdstuk5;

/**
 * Created By Moon
 * 1/8/2021, Fri
 **/
public class LogicalOperators {
    public static void main(String[] args) {
        boolean itRains = true;
        boolean isSummer = false;
        boolean inBelgiumLive = true;
        boolean isRequiredGoOut = false;

        if (isSummer){
            System.out.println("I will go holiday!");
            if(itRains && inBelgiumLive){
                System.out.println("Don't have to take an umbrella");
            }else if(itRains || isRequiredGoOut){
                System.out.println("Sleep :)");
            }
        }else {
            System.out.println("I stay home!");
            if(itRains && inBelgiumLive && !isRequiredGoOut){
                System.out.println("I should take an umbrella");
            }
        }
    }
}
