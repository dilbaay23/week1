package hoofdstuk5;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created By Moon
 * 1/11/2021, Mon
 **/
public class ScrabbleChar {
    public static void main(String[] args) throws IOException {
        Scanner keyboard = new Scanner(System.in);

        while(true){

            System.out.println("Geef een letter aub: ");
            char letter = keyboard.nextLine().toUpperCase().charAt(0);

            int value;
            switch (letter){
                case 'A':
                case 'E':
                case 'I':
                case 'L':
                case 'N':
                case 'O':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                    value = 1;
                    break;
                case 'D':
                case 'G':
                    value = 2;
                    break;
                case 'B':
                case 'C':
                case 'M':
                case 'P':
                    value = 3;
                    break;
                case 'F':
                case 'H':
                case 'V':
                case 'W':
                case 'Y':
                    value= 4;
                    break;
                case 'K':
                    value = 5;
                    break;
                case 'J':
                case 'X':
                    value = 8;
                    break;
                case 'Q':
                case 'Z':
                    value = 10;
                    break;
                default:
                    value = 0;
                    break;
            }

            System.out.printf("The score for the %s is : %d%n", letter, value);
            System.out.println("Dou you wanna go continue (y/n)");
            char cont = keyboard.nextLine().toLowerCase().charAt(0);
            if(cont=='n'){
                break;
            }
            keyboard.close();
        }
}

}