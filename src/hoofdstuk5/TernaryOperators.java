package hoofdstuk5;

/**
 * Created By Moon
 * 1/8/2021, Fri
 **/
public class TernaryOperators {
    public static void main(String[] args) {
       int a = 40;
       int b = 11;
       int c = -9;

        System.out.println((a < b) ? a : b);
        System.out.println((a < c) ? a : c);
        System.out.println((b < a) ? b : a);
        System.out.println((b < c) ? b : c);
        System.out.println((c < a) ? c : a);
        System.out.println((c < b) ? c : b);

    }
}
