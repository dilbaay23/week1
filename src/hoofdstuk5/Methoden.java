package hoofdstuk5;

/**
 * Created By Moon
 * 1/11/2021, Mon
 **/
public class Methoden {
    public static void main(String[] args) {
        greetWorld();
        greet("Moon");
        System.out.println(add(23,14));
        System.out.println(sub(20,2));
        System.out.println(multiple(2,14));
        System.out.println(division(23,5));
    }
    public static void greetWorld(){
        System.out.println("Hello World");
    }

    public static void greet(String recipient){
        System.out.println("Hello " + recipient);
    }

    public static int add(int a, int b){
        return a + b;
    }

    public static int sub(int a, int b){
        return a - b;
    }

    public static int multiple(int a, int b){
        return a * b;
    }

    public static double division(int a, int b){
        return (double)a / b;
    }
}
