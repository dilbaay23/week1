package hoofdstuk5;

/**
 * Created By Moon
 * 1/11/2021, Mon
 **/
public class ForStatement {
    public static void main(String[] args) {

/*        for (int i = 399; i > 350; i--) {
            System.out.println(i);
        }*/

     /*   for (int i = 7; i < 200; i +=7) {
            System.out.println(i);
        }*/


/*        for (int result = 11; result < 100_000;) {
            System.out.println(result);
            result *=11;
        }*/

/*        for (char i='z' ; i >='a' ; i--) {
            System.out.println(i);
        }*/

/*        for (int i = -10; i <=10 ; i++) {
            if (i > 0) System.out.println("+" + i);
            else
            System.out.println(i);
        }*/

/*        for (int i = -10; i <= 10 ; i++) {
            String counter = i > 0 ? "+"+i : ""+i;
            System.out.println(counter);
        }*/

      /*  for (int i = 0; i < 10_000; i++) {
            if(i % 6 == 0 && i % 28 == 0) System.out.println(i);
        }*/

        for (int i = 2; i < 1000 ; i++) {
            boolean flag = true;
            for (int j = 2; j < i ; j++) {
                if (i % j == 0) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                System.out.println(i);
        }


    }
}
