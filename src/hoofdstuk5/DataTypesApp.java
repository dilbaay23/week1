package hoofdstuk5;

/**
 * Created By Moon
 * 1/7/2021, Thu
 **/

public class DataTypesApp {
    public static void main(String[] args) {
        boolean aBoolean = false;
        char aCharacter = 'd';
        byte aByte = 126;
        short aShortInteger = 115;
        int anInteger = 1256456;
        long aLongInteger = 45631341L;
        float aDecimalNumber = 1256.32F;
        double aBigDecimalNumber = 12.365987451236;
        int anOctaleInt = 0342;
        int aHexInteger = 0X56_31;
        int aBinInteger = 0b0101_1100;

        aByte= (byte) aShortInteger;
        aShortInteger= (short) aCharacter;
        anInteger = aCharacter;

        System.out.println(aBoolean);
        System.out.println(aCharacter);
        System.out.println(aByte);
        System.out.println(aShortInteger);
        System.out.println(anInteger);
        System.out.println(aLongInteger);
        System.out.println(aDecimalNumber);
        System.out.println(aBigDecimalNumber);
        System.out.println(anOctaleInt);
        System.out.println(aHexInteger);
        System.out.println(aBinInteger);
    }
}
