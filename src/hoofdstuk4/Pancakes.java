package hoofdstuk4;

/**
 * Created By Moon
 * 1/7/2021, Thu
 **/
public class Pancakes {
    public static void main(String[] args) {
        System.out.println("Take flour");
        System.out.println("Add milk");
        System.out.println("Add eggs");
        System.out.println("Mix ingredients");
        System.out.println("...........................");

        for (int count=1; count < 10; count++ ){
            System.out.printf("You baked %d pancakes\n", count);
        }

    }
}
