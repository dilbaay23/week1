package hoofdstuk4;

import java.util.Random;
import java.util.Scanner;

/**
 * Created By Moon
 * 1/7/2021, Thu
 **/

public class HigherLowerGame {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        Random randomNumberGenerator = new Random();

        int  numberToGuess = randomNumberGenerator.nextInt(100);

        System.out.println("Probeer mijn getal te reden: ");
        boolean guessed = false;

        do{
            int guess = keyboard.nextInt();
            if (guess > numberToGuess){
                System.out.println("Hoger! ");
            }else if(guess < numberToGuess){
                System.out.println("Lager! ");
            }else {
                System.out.println("Bingo! Je hebt hem ! Mijn getal was " + numberToGuess);
                guessed = true;
            }
        }while(guessed == false);

        keyboard.close();
    }

}
